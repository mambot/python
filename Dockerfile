ARG PYTHON_V
FROM python:$PYTHON_V
ARG PYTHON_V=$PYTHON_V
RUN echo "PYTHON_V" $PYTHON_V
RUN echo "PYTHON_VERSION" $PYTHON_VERSION

WORKDIR /usr/src/app

COPY requirements.$PYTHON_V.txt ./
RUN pip install --no-cache-dir -r requirements.$PYTHON_V.txt

COPY . .
RUN chmod +x ./*.py