# Python with Docker

How use this image Docker to have Python differents versions.

## Create requirements file
1. Export python version with `export PYTHON_V=3.7`
2. Create a requirements file with `cp ./requirements.dist ./requirements.$PYTHON_V.txt`

## Build image Python
`docker build -t python:$PYTHON_V --build-arg PYTHON_V=$PYTHON_V .`

## Execute your Python script in container with built image
`docker run -it python:$PYTHON_V python test.py 1 2 3 --sum`